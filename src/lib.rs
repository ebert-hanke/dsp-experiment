use cpal::traits::{DeviceTrait, StreamTrait};
use dasp::{
    signal::{self as dasp_signal, ConstHz, Sine},
    Sample, Signal,
};
use std::sync::mpsc;

pub struct Synth {
    pub signal: Option<dasp_signal::Take<Sine<ConstHz>>>,
}
impl Synth {
    pub fn new() -> Self {
        Self { signal: None }
    }
    pub fn set_sine(&mut self, sample_rate: f64, time: usize, freq: f64) {
        let time = (sample_rate / 1000.0) as usize * time;
        self.signal = Some(
            dasp_signal::rate(sample_rate)
                .const_hz(freq)
                .sine()
                .take(time),
        );
    }
}
impl Default for Synth {
    fn default() -> Self {
        Self::new()
    }
}

/// just for finding out whats what :)
fn print_type_of<T>(_: &T) {
    println!("{}", std::any::type_name::<T>())
}

pub fn run<T>(
    device: &cpal::Device,
    config: &cpal::StreamConfig,
    synth: Synth,
    //    synth_signal: &mut dasp_signal::Take<Sine<ConstHz>>,
) -> Result<(), anyhow::Error>
where
    T: cpal::Sample,
{
    // A channel for indicating when playback has completed.
    let (complete_tx, complete_rx) = mpsc::sync_channel(1);
    let mut samples = synth.signal.unwrap().map(|s| s.to_sample::<f32>() * 0.2);
    print_type_of(&samples);
    // Create and run the stream.
    let err_fn = |err| eprintln!("an error occurred on stream: {}", err);
    let channels = config.channels as usize;
    let stream = device.build_output_stream(
        config,
        move |data: &mut [T], _: &cpal::OutputCallbackInfo| {
            write_data(data, channels, &complete_tx, &mut samples)
        },
        err_fn,
    )?;
    stream.play()?;

    // Wait for playback to complete.
    complete_rx.recv().unwrap();
    stream.pause()?;
    Ok(())
}

/// Write signal to audio output buffer.
fn write_data<T>(
    output: &mut [T],
    channels: usize,
    complete_tx: &mpsc::SyncSender<()>,
    signal: &mut dyn Iterator<Item = f32>,
) where
    T: cpal::Sample,
{
    for frame in output.chunks_mut(channels) {
        let sample = match signal.next() {
            None => {
                complete_tx.try_send(()).ok();
                0.0
            }
            Some(sample) => sample,
        };
        let value: T = cpal::Sample::from::<f32>(&sample);
        for sample in frame.iter_mut() {
            *sample = value;
        }
    }
}
