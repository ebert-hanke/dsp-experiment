use cpal::traits::{DeviceTrait, HostTrait};
use dsp_experiment::{run, Synth};

fn main() -> Result<(), anyhow::Error> {
    let mut synth = Synth::new();
    synth.set_sine(44000.0, 2000, 440.0);

    let host = cpal::default_host();
    let device = host
        .default_output_device()
        .expect("no output device available");

    let mut supported_configs_range = device
        .supported_output_configs()
        .expect("error while querying config");

    let supported_config = supported_configs_range
        .next()
        .expect("no supported config?!")
        .with_max_sample_rate();
    // .with_sample_rate(SampleRate(44_100));

    let sample_format = supported_config.sample_format();
    let config: cpal::StreamConfig = supported_config.into();
    let _stream = match sample_format {
        cpal::SampleFormat::F32 => run::<f32>(&device, &config, synth)?,
        cpal::SampleFormat::I16 => run::<i16>(&device, &config, synth)?,
        cpal::SampleFormat::U16 => run::<u16>(&device, &config, synth)?,
    };
    Ok(())
}
