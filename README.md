# DSP Experiment

We are building a soft-synth :)

## TODOs

- [ ] Clean up code (remove unused stuff)
- [ ] Replace portaudio for [cpal](https://github.com/RustAudio/cpal)
- [ ] Use [midir](https://github.com/Boddlnagg/midir) to play notes on midi input messages
- [ ] Use [pitch_calc](https://github.com/RustAudio/pitch_calc) to convert midi steps to frequencies
- [ ] Modularize code
- [ ] Close program via midi control message
- [ ] Build a simple GUI, probably with [tauri](https://tauri.studio/en/)


# Open questions

- [ ] How does `into()` really work with SupportedStreamConfig -> StreamConfig
